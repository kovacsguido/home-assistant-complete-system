# Komplett otthoni rendszer

Komplett Home Assistant rendszer kialakítása Docker Compose segítségével (Home Assistant, Configurator, Mosquitto, MySQL, Samba, Portainer, Plex).

## Előzetes tudnivalók

A project a "/srv" mappába lett tervezve, ezért ha ettől eltérő helyre szeretnéd telepíteni, akkor az összes vonatkozó elérési utat át kell írni.

## Előfeltételek

A rendszer kialakításához az alábbi feltételek szükségesek a számítógépen:

* alap Ubuntu telepítése (lehetőleg 20.04, grafikus felület nem kell)
* git telepítése (`sudo apt install git`)
* docker-ce telepítése (https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04)
* docker-compose telepítése (https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04)
* valamilyen szövegszerkesztő telepítése (pl.: `sudo apt install mc` - ez a Midnight Commanderrel együtt az mcedit szövegszerkesztőt is telepíti)

## Telepítés

Az alábbi lépések végrehajtásával telepítheted a projectet (ha nem az "/srv" könyvtárba akarod telepíteni, akkor értelemszerűen módosítsd az adott részt):

```
cd /srv
sudo git clone https://bitbucket.org/kovacsguido/home-assistant-complete-system.git
sudo mv /srv/home-assistant-complete-system/* /srv
sudo rm -rf /srv/home-assistant-complete-system
```

## Beállítás - Konténerek

A telepítés befejeződött, de néhány beállítást még meg kell ejtened a docker-compose.yaml fájlban, ezek pedig a következők.

### Mosquitto

Első lépésként két könyvtárnál szükség van a jogosultságok módosítására, hogy a Mosquitto tudja írni a mappákat. Ehhez futtassuk le az alábbi parancsokat:

```
sudo chmod 777 /srv/docker/mosquitto/data
sudo chmod 777 /srv/docker/mosquitto/log
```

Valamint létre kell hoznunk egy üres password fájlt, mert a Mosquitto ezt induláskor keresni fogja és ha nem találja, nem indul el. A fájl létrehozásához futtassuk le az alábbi parancsot:

```
sudo touch /srv/docker/mosquitto/config/pwfile
```

Felhasználót és jelszót majd később adunk hozzá, ha már elindult a Mosquitto.

### Samba

* a USERNAME és a PASSWORD szavak lecserélése a Windowsos megosztást használó felhasználó belépési adataira
* a WORKGROUP szó lecserélése az átalad használt munkacsoport nevére
* ha a Samba konténerhez csatolni kívánt mappa nem az "/srv/data/samba", akkor ennek az értelemszerű módosítása a "volumes" alatt

### Plex

* a PLEXTOKEN szó lecserélése a saját telepített Plex-ed tokenjére

### MySQL

* a PASSWORD szó lecserélése a kívánt jelszóra (ezzel fog kapcsolódni a HA az adatbázishoz)
* a ROOTPASSWORD szó lecserélése a kívánt adatbázis-rendszergazdai jelszóra

## Rendszer elindítása

Ha a fentiekkel elkészültél nem marad más hátra, mint elindítani a konténereket az alábbi parancs futtatásával:

```
sudo docker-compose -f /srv/docker-compose.yaml up -d
```

Az első rendszerindítás - számítógéptől függően - 4-5 perctől akár 30 percig is terjedhet. Ennek az az oka, hogy a Home Assistant az első indításnál sok Python függőséget telepít "magára".
Tehát az előzőek miatt ne aggódj azon, ha az indítás után egy ideig még nem jelenik meg a Home Assistant a böngészőablakban.

## Beállítás - Home Assistant

Ha kivártuk az előző bekezdésben jelzett időt és a Home Assistant elindult, akkor ellenőrizzük le, hogy minden szolgáltatásunk fut-e. Erre azért van szükség, hogy csak biztosan működő szolgáltatásokat adjunk hozzá a Home Assistanthoz.
Ehhez futtassuk le a `sudo docker-compose -f /srv/docker-compose.yaml ps ` parancsot. Ha minden szolgáltatás sorában a futási állapot oszlopban az "Up" szót látjuk, akkor minden rendben van.

### Mosquitto felhasználó hozzáadása

Most adjuk hozzá a felhasználónév/jelszó párost, ezzel fog tudni majd hozzákapcsolódni mind a Home Assistant, mind pedig az egyes szenzorok ill. aktorok. A felhasználó hozzáadása (a Linux host rendszeren) az alábbi paranccsal történik:

```
sudo docker-compose mqtt exec /bin/bash -c "mosquitto_passwd -b /mosquitto/config/pwfile homeassistant PASSWORD"
```

A fenti parancs létrehozza a passwordfájlt, hozzáadja a "homeassistant" felhasználót a kért jelszóval (amit értelemszerűen a PASSWORD helyére kell beírni).
Ha esetleg további felhasználókat is hozzá akarsz adni, akkor azt az alábbi paranccsal teheted meg:

```
sudo docker-compose mqtt exec /bin/bash -c "mosquitto_passwd -b /mosquitto/config/pwfile [username] [password]"
```

### HASS Configurator hozzáadása

Ehhez a Home Assistant fő konfigurációs fájlját, a "configuration.yaml" fájlt kell szerkesztened (a Linux host rendszeren add ki az `sudo mcedit /srv/docker/home-assistant/configuration.yaml` parancsot). A fájl végéhez add hozzá az alábbi sorokat (ügyelj a behúzásokra, tudod, YAML fájl).

```
panel_iframe:
  configurator:
    title: Configurator
    icon: mdi:wrench
    url: http://1.2.3.4:3218
```

Értelemszerűen a fenti 1.2.3.4 IP cím helyére a te számítógéped IP címe kerüljön. Mentsd el a fájlt és indítsd újra a Home Assistant konténerét (a Linux host rensdszeren) az alábbi parancs lefuttatásával:

```
sudo docker-compose -f /srv/docker-compose.yaml restart hass
```

A konténer (és a Home Assistant) újraindítása 10-20 másodpercet is igénybe vehet, ez alatt az idő alatt a Home Assistant ablakának bal alsó sarkában egy újrakapcsolódást jelző üzenet látható. Ha ez eltűnik, akkor fejeződött be az újraindítás.

### MQTT hozzáadása

Korábbi rendszereknél ehhez is a Home Assistant konfigurációs fájlját kellett kézzel szerkeszteni, de szerencsére erre itt már nincs szükség. Töltsd be egy böngészőbe a Home Assistantot, majd a bal oldali menüben kattints a "Configuration", menüpontra, majd azon belül az "Integrations" gombra. Itt kattints alul a kör alakú "+" jelet tartalmazó gombra, válaszd ki az MQTT opciót, majd add meg a kért adatokat az alábbiak szerint (értelemszerűen a MQTTUSERNAME és az MQTTPASSWORD szavak helyett a megfelelő adatokat add meg, azaz azokat, amelyeket pár perccel korábban a Mosquittonak beállítottál):

```
Broker: 127.0.0.1
Port: 1883
Username: MQTTUSERNAME
Password: MQTTPASSWORD
```

Megjegyzés: Az integráció végrehajtása gengébb gépen akár 20-30 másodpercet is igénybe vehet.

### MySQL hozzáadása

Sajnos itt sem ússzod meg a konfigurációs fájl szerkesztését, de itt már használhatod a Configuratort. Nyisd meg vele a configuration.yaml fájlt és a végére szúrd be az alábbi sorokat:

```
recorder:
  db_url: mysql://hauser:MYSQLPASSWORD@1.2.3.4/ha?charset=utf8
  purge_keep_days: 7
  exclude:
    domains:
      - automation
      - updater
    entities:
      - sun.sun
```

A fenti konfigurációban látható "exclude" szekcióban tudunk akár egyes entitásokat (entities rész), akár komplett kategóriákat (domains rész) kihagyni a logolásból. A fenti beállítás 7 napig örzi meg a logbejegyzéseket.
###
Értelemszerűen a fenti 1.2.3.4 IP cím helyére a te számítógéped IP címe kerüljön, valamint a MYSQLPASSWORD szó helyére az a jelszó, amelyet a docker-compose.yaml fájlban korábban a MYSQL_PASSWORD sorban az egyenlőségjel után megadtál.
###
Ha ezzel megvagy, akkor ellenőrizd a konfiguráció helyességét (Konfiguráció > Általános > Konfiguráció ellenőrzése) és ha minden rendben van, akkor az alábbi parancs futtatásával ismét indítsd újra a Home Assistant konténerét.

```
sudo docker-compose -f /srv/docker-compose.yaml restart hass
```
